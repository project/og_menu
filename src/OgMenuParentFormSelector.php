<?php

namespace Drupal\og_menu;

use Drupal\Core\Menu\MenuParentFormSelector;

/**
 * OG menu parent selector.
 */
class OgMenuParentFormSelector extends MenuParentFormSelector {

  /**
   * Whether this is an OG menu.
   */
  protected bool $isOgMenu = FALSE;

  /**
   * {@inheritdoc}
   */
  public function parentSelectElement($menu_parent, $id = '', ?array $menus = NULL) {
    if (strpos($menu_parent, 'ogmenu-') !== FALSE) {
      $this->isOgMenu = TRUE;
    }
    return parent::parentSelectElement($menu_parent, $id, $menus);
  }

  /**
   * {@inheritdoc}
   */
  protected function getMenuOptions(?array $menu_names = NULL) {
    $entity_type = 'menu';
    if ($this->isOgMenu) {
      $entity_type = 'ogmenu_instance';
    }

    // @todo Here we'll need to do some access checks to see if which menus
    //   belong to the og.
    $menus = $this->entityTypeManager->getStorage($entity_type)->loadMultiple($menu_names);
    $options = [];
    /** @var \Drupal\system\MenuInterface[] $menus */
    foreach ($menus as $menu) {
      if ($this->isOgMenu) {
        $options['ogmenu-' . $menu->id()] = $menu->label();
      }
      else {
        $options[$menu->id()] = $menu->label();
      }

    }
    return $options;
  }

}
