# OG Menu
[![https://drupalcode.org/project/og_menu/badges/2.x/pipeline.svg](https://drupalcode.org/project/og_menu/badges/2.x/pipeline.svg)](https://drupalcode.org/project/og_menu)

Enable users to manage menus inside Organic Groups.

## Requirements
- Organic Groups module (http://drupal.org/project/og).
- Menu module.

## Installation

- Enable the module.
- Give "administer og menu" permission to the desired roles.
- Visit the og menu configuration page to configure at will.
- Enable group content types for use with OG Menu.

## Usage

- Administrators can create OG menus through the regular menu interface at
  `admin/structure/menu/add`. Choose a group to associate with the menu.
- Organic group members with the "administer og menu" permission can also manage
  menus at `node/[nid]/og_menu`.
- "administer og menu" permission can be granted on global or group level.
- Group content types can be enabled for use with OG Menu. Once enabled, users
  can add a menu link directly from the node creation form to any of the menu's
  they have access to.
- For group types, users can create an associated menu by checking
  "Enable menu for this group" on the node edit/creation form.
- You can enable the "OG Menu : single" and the "OG Menu : multiple" blocks at
  `admin/build/block`.
  - "OG Menu : single" will display the first available menu for the first
    available group in the context.
  - "OG Menu : multiple" will display all available menus for all available
    groups in the context.
- OG menus won't show on the regular menu interface. They show up on
  `admin/structure/og_menu`.
- Ability to hide OG Menu's from the block admin interface and on other places
  for some contrib modules.

## Notes

Be aware that since menu administration forms are mostly duplicated, if a
contrib module adds functionality to menu administration forms without
additional permissions, these additions may be available for OG menu users with
`administer og menu` permission. This could allow these users to be able to do
things you don't want them to. Please report these modules if you catch one.

## ToDo / Bugs / Feature requests

See http://drupal.org/project/issues/og_menu. Please search before filing issues
in order to prevent duplicates.

## Contributing

[DDEV](https://ddev.com), a Docker-based PHP development tool for a streamlined
and unified development process, is the recommended tool for contributing to the
module. The [DDEV Drupal Contrib](https://github.com/ddev/ddev-drupal-contrib)
addon makes it easy to develop a Drupal module by offering the tools to set up
and test the module.

### Install DDEV

* Install a Docker provider by following DDEV [Docker Installation](https://ddev.readthedocs.io/en/stable/users/install/docker-installation/)
  instructions for your Operating System.
* [Install DDEV](https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/),
  use the documentation that best fits your OS.
* DDEV is used mostly via CLI commands. [Configure shell completion &
  autocomplete](https://ddev.readthedocs.io/en/stable/users/install/shell-completion/)
  according to your environment.
* Configure your IDE to take advantage of the DDEV features. This is a critical
  step to be able to test and debug your module. Remember, the website runs
  inside Docker, so pay attention to these configurations:
    - [PhpStorm Setup](https://ddev.readthedocs.io/en/stable/users/install/phpstorm/)
    - [Configure](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/step-debugging/)
      PhpStorm and VS Code for step debugging.
    - Profiling with [xhprof](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xhprof-profiling/),
      [Xdebug](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xdebug-profiling/)
      and [Blackfire](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/blackfire-profiling/).

### Checkout the module

Normally, you check out the code form an [issue fork](https://www.drupal.org/docs/develop/git/using-gitlab-to-contribute-to-drupal/creating-issue-forks):

```shell
git clone git@git.drupal.org:issue/og_menu-[issue number].git
cd og_menu-[issue number]
```

### Start DDEV

Inside the cloned project run:

```shell
ddev start
```

This command will fire up the Docker containers and add all configurations.

### Install dependencies

```shell
ddev poser
```

This will install the PHP dependencies. Note that this is a replacement for
Composer _install_ command that knows how to bundle together Drupal core and the
module. Read more about this command at
https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands

```shell
ddev symlink-project
```

This symlinks the module inside `web/modules/custom`. Read more about this
command at https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands.
Note that as soon as `vendor/autoload.php` has been generated, this command runs
automatically on every `ddev start`.

This command should also be run when adding new directories or files to the root
of the module.

```shell
ddev exec "cd web/core && yarn install"
```

Install Node dependencies. This is needed for the `ddev eslint` command.

### Install Drupal

```shell
ddev install
```

This will install Drupal and will enable the module.

### Changing the Drupal core version

* Create a file `.ddev/config.local.yaml`
* Set the desired Drupal core version. E.g.,
  ```yaml
  web_environment:
    - DRUPAL_CORE=^10.3
  ```
* Run `ddev restart`

### Run tests

* `ddev phpunit`: run PHPUnit tests
* `ddev phpcs`: run PHP coding standards checks
* `ddev phpcbf`: fix coding standards findings
* `ddev phpstan`: run PHP static analysis
* `ddev eslint`: Run ESLint on Javascript and YAML files.

## Credits

Originally authored and maintained by Scott Ash (ashsc).

7.x-3.x port contributors (sorry if anyone was forgotten):
  - bulldozer2003
  - zipymonkey
  - jgraham
  - Jackinloadup
  - Wim Vanheste (rv0) (http://www.coworks.net)

7.x initial port contributors:
  - Stefan Vaduva (http://vamist.ro)
  - Nick Santamaria (http://www.itomic.com.au)
  - Frederik Grunta (Aeternum)
  - Wim Vanheste (rv0) (http://www.coworks.net)

7.x maintainers
  - Wim Vanheste (rv0) (http://www.coworks.net)

6.x-2.x maintainer
  - Julien De Luca (jide)
